
# Создаем сервер Docker
$ docker-machine create \
	-d generic \
    --generic-ip-address 192.168.12.130 \
    --generic-ssh-key ~/.ssh/id_rsa_server-1 \
    --generic-ssh-user pavel-karpukhin \
    swarm-1

# Его окружение
$ docker-machine env swarm-1

# Создаем кластер, начиная с первой ноды
$ eval $(docker-machine env swarm-1)

$ docker swarm init --advertise-addr $(docker-machine ip swarm-1)


$ eval $(docker-machine env swarm-1)

# Создаем необходимые директории для Jenkins
$ sudo mkdir -pv /data/jenkins && sudo chmod 777 /data/jenkins

# Создаем Jenkins master сервис
$ docker service create \
        --name jenkins \
        -p 8082:8080 \
        -p 50000:50000 \
        -e JAVA_OPTS=-Duser.timezone=Europe/Moscow \
        -e JENKINS_OPTS="--prefix=/jenkins" \
        --mount "type=bind,source=/data/jenkins,target=/var/jenkins_home" \
        --reserve-memory 300m \
        jenkinsci/blueocean:1.10.1

# Создаем необходимые директории для Jenkins агента
$ sudo mkdir -pv /workspace && sudo chmod 777 /workspace
$ sudo mkdir -pv /machines && sudo chmod 777 /machines

# Создаем Jenkins slave сервис
$ docker service create \
        --name jenkins-agent \
        -e COMMAND_OPTIONS="-master \
        http://$(docker-machine ip swarm-1):8082/jenkins \
        -username pavel-karpukhin -password P@ssw0rd1+ \
        -labels 'docker' -executors 5" \
        --mode global \
        --mount "type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock" \
        --mount "type=bind,source=/workspace,target=/workspace" \
        --mount "type=bind,source=/machines,target=/machines" \
        vfarcic/jenkins-swarm-agent:18.04.01-1

#softonic/jenkins-swarm-agent:0.9.0

# Создаем необходимые директории для Docker Registry
$ sudo mkdir -pv /data/registry && sudo chmod 777 /data/registry

# Создаем Docker Registry сервис
$ docker service create \
        --name registry \
        -p 5000:5000 \
        --reserve-memory 100m \
        --mount "type=bind,source=/data/registry,target=/var/lib/registry" \
        registry:2.7.1


$ docker network create -d overlay --attachable proxy
$ docker network create -d overlay --attachable monitor
$ docker network create -d overlay --attachable app

$ docker stack deploy \
        -c docker/docker-flow-proxy.yml \
        proxy

$ DOMAIN=192.168.12.130 \
        docker stack deploy \
        -c docker/docker-flow-monitor-proxy.yml \
        monitor

$ docker stack deploy \
        -c docker/exporters.yml \
        exporter


$ export DOCKER_HOST="tcp://192.168.12.130:2376"
$ export DOCKER_CERT_PATH="/home/pavel-karpukhin/.docker/machine/machines/swarm-1"

$ curl -fsS \
    --cert $DOCKER_CERT_PATH/cert.pem \
    --key $DOCKER_CERT_PATH/key.pem \
    --cacert $DOCKER_CERT_PATH/ca.pem \
    https://$(docker-machine ip swarm-1):2376/images/json

$ docker service create \
    --name tomcat-blue \
    -l blueGreen.serviceName=tomcat \
    -l blueGreen.color=blue \
    tomcat:7-jre8-alpine

$ docker service create \
    --name tomcat-green \
    -l blueGreen.serviceName=tomcat \
    -l blueGreen.color=green \
    tomcat:7-jre8-alpine

$ curl -fgsS \
    --cert $DOCKER_CERT_PATH/cert.pem \
    --key $DOCKER_CERT_PATH/key.pem \
    --cacert $DOCKER_CERT_PATH/ca.pem \
    https://$(docker-machine ip swarm-1):2376/services?filters='{"name":["tomcat-blue"]}' | jq '.[].Spec.Labels'

$ curl -fgsS \
    --cert $DOCKER_CERT_PATH/cert.pem \
    --key $DOCKER_CERT_PATH/key.pem \
    --cacert $DOCKER_CERT_PATH/ca.pem \
    https://$(docker-machine ip swarm-1):2376/services?filters='{"label":["blueGreen.serviceName=tomcat"]}' | jq '.[].Spec.Labels'

$ curl -fgsS \
    --cert $DOCKER_CERT_PATH/cert.pem \
    --key $DOCKER_CERT_PATH/key.pem \
    --cacert $DOCKER_CERT_PATH/ca.pem \
    https://$(docker-machine ip swarm-1):2376/services?filters='{"label":["blueGreen.serviceName=tomcat"]}' | jq '.[].Spec.Labels."blueGreen.color"'

$ docker run --rm appropriate/curl \
    -fgsS \
    --cert $DOCKER_CERT_PATH/cert.pem \
    --key $DOCKER_CERT_PATH/key.pem \
    --cacert $DOCKER_CERT_PATH/ca.pem \
    https://$(docker-machine ip swarm-1):2376/services?filters='{"label":["blueGreen.serviceName=tomcat"]}'

$ docker run --rm \
    --mount "type=bind,source=/opt/vesta/machines,target=/machines" \
    appropriate/curl \
    -fgsS \
    --cert /machines/swarm-1/cert.pem \
    --key /machines/swarm-1/key.pem \
    --cacert /machines/swarm-1/ca.pem \

$ docker run --rm \
    --network proxy \
    appropriate/curl \
    -fgsS \
    http://proxy:8080/v1/docker-flow-proxy/reconfigure?serviceName=external-service-blue&servicePath=/external-service&port=8080

$ curl -fgsS http://192.168.153.129:8080/v1/docker-flow-proxy/reconfigure?serviceName=external-service-blue&servicePath=/external-service&port=8080

# Создаем сервер Docker
$ docker-machine create \
	-d generic \
    --generic-ip-address 192.168.12.132 \
    --generic-ssh-key ~/.ssh/id_rsa_server-1 \
    --generic-ssh-user pavel-karpukhin \
    swarm-2

# Prometheus
$ docker run -d \
        --name prometheus \
        --memory 512MB \
        --mount type=bind,source=/data/prometheus,target=/prometheus \
        --network app \
        --publish 9090:9090 \
        --restart unless-stopped \
        prom/prometheus:v2.7.1 \
        --storage.tsdb.path=/prometheus \
        --web.external-url=http://192.168.12.130/prometheus

# Grafana
$ docker run -d \
        --name grafana \
        --env 'GF_SERVER_DOMAIN=192.168.12.130' \
        --env 'GF_SERVER_ROOT_URL=%(protocol)s://%(domain)s:%(http_port)s/grafana' \
        --env 'GF_SECURITY_ADMIN_USER=admin' \
        --env 'GF_SECURITY_ADMIN_PASSWORD=P@ssw0rd1+' \
        --memory 512MB \
        --mount type=bind,source=/data/grafana,target=/var/lib/grafana \
        --network app \
        --publish 3000:3000 \
        --restart unless-stopped \
        grafana/grafana:5.4.3
