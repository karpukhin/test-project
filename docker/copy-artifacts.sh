#!/bin/sh

PROJECT_NAME=clientone

CONTAINER_1=`docker-compose -p $PROJECT_NAME ps | tail -n +3 | awk '{print $1}' | grep external-service`
CONTAINER_2=`docker-compose -p $PROJECT_NAME ps | tail -n +3 | awk '{print $1}' | grep internal-service`

docker cp ../external-service/target/external-service-0.1-SNAPSHOT.war \
	$CONTAINER_1:/opt/cool-service/external-service/webapps/external-service.war
docker cp ../internal-service/target/internal-service-0.1-SNAPSHOT.war \
	$CONTAINER_2:/opt/cool-service/internal-service/webapps/internal-service.war
