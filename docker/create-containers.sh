#!/bin/sh

PROJECT_NAME=client-one

docker-compose \
	-p $PROJECT_NAME \
	up \
	--build \
	--no-start
