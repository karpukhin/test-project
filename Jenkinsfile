node("docker") {

    def jobParams = [
            'git.branch': 'master',
            'skip.unit': false,
            'skip.test': false
    ]

    for (def param in params) {
        jobParams[param.key] = param.value
    }
    echo "Job params: ${jobParams}"

    def artifacts = [
            "external-service",
            "internal-service"
    ]

    stage("Pull") {
        //git url: "https://karpukhin@bitbucket.org/karpukhin/test-project.git", credentialsId: "karpukhin_at_bitbucket.org", branch: "${params['git.branch']}"
        checkout scm: [$class: 'GitSCM', branches: [[name: jobParams['git.branch']]], clean: true,
                       userRemoteConfigs: [[credentialsId: 'karpukhin_at_bitbucket.org', url: 'https://karpukhin@bitbucket.org/karpukhin/test-project.git']]]
    }

    timestamps {
        stage("Unit") {
            if (Boolean.valueOf(jobParams['skip.unit'])) {
                echo "Unit was skipped"
            } else {
                timeout (time: 45, unit: 'MINUTES') {
                    try {
                        sh "docker run --rm \
                            -v \"\$HOME/.m2\":/root/.m2 -v \"\$PWD\":/usr/src/app \
                            -w /usr/src/app maven:3.6.0-jdk-8-alpine \
                            mvn -B -Duser.timezone=Europe/Moscow clean verify"
                    } finally {
                        junit testResults: '**/target/surefire-reports/*.xml', allowEmptyResults: true
                    }
                }
            }
        }

        withEnv([
                "COMPOSE_FILE=docker/docker-compose-test-images.yml",
                "COMPOSE_PROJECT_NAME=test"
        ]) {
            stage("Artifacts") {
                if (Boolean.valueOf(jobParams['skip.test'])) {
                    echo "Artifacts was skipped"
                } else {
                    try {
                        sh "docker-compose --no-ansi run --rm artifacts"

                        for (def artifact in artifacts) {
                            sh script: "docker image rm ${artifact}:test", returnStatus: true
                            sh "docker-compose --no-ansi build ${artifact}-image"
                        }
                    } catch (e) {
                        for (def artifact in artifacts) {
                            sh script: "docker image rm ${artifact}:test", returnStatus: true
                        }
                        error "Error: ${e}"
                    }
                }
            }
        }

        withEnv([
                "COMPOSE_FILE=docker/docker-compose-test-local.yml",
                "COMPOSE_PROJECT_NAME=staging"
        ]) {
            stage("Staging") {
                if (Boolean.valueOf(jobParams['skip.test'])) {
                    echo "Staging was skipped"
                } else {
                    try {
                        sh "docker-compose --no-ansi up -d --build external-service"
                        sh "docker-compose --no-ansi up -d --build internal-service"
                        sleep time: 10, unit: "SECONDS"
                        sh "docker-compose --no-ansi run --rm test"
                    } catch (e) {
                        sh "docker-compose --no-ansi logs"
                        error "Staging failed, error: ${e}"
                    } finally {
                        sh "docker-compose --no-ansi down"

                        for (def artifact in artifacts) {
                            sh script: "docker image rm ${artifact}:test", returnStatus: true
                        }
                    }
                }
            }
        }
    }
}