@Library('jenkins-shared-library') _

node("docker") {
    properties([
            parameters([
                    string(name: 'git.branch', defaultValue: 'refs/tags/0.1', description: 'Environment config'),
                    string(name: 'environment.config', defaultValue: 'uat', description: 'Git branch'),
                    string(name: 'docker.host.ip', defaultValue: '172.16.104.63', description: 'Swarm manager IP'),
                    string(name: 'docker.mount.root', defaultValue: '/opt/vesta/docker/new', description: 'Docker mount root'),
                    string(name: 'docker.registry.host.port', defaultValue: 'localhost:5000', description: 'Docker registry host and port')
            ])
    ])
    stage("Pull") {
        //git url: "https://karpukhin@bitbucket.org/karpukhin/test-project.git", credentialsId: "karpukhin_at_bitbucket.org", branch: "${params['git.branch']}"
        checkout scm: [$class: 'GitSCM', branches: [[name: params['git.branch']]], clean: true,
                       userRemoteConfigs: [[credentialsId: 'karpukhin_at_bitbucket.org', url: 'https://karpukhin@bitbucket.org/karpukhin/test-project.git']]]
    }
    def artifacts = [
            "external-service",
            "internal-service"
    ]

    def mavenProjectProperties = ""
    for (def param in params) {
        mavenProjectProperties += " -D${param.key}=${param.value}"
    }
    echo "Maven project props: ${mavenProjectProperties}"

    timestamps {
        withEnv([
                "COMPOSE_FILE=docker/docker-compose-test-images.yml",
                "COMPOSE_PROJECT_NAME=test"
        ]) {
            stage("Unit") {
                sh "docker-compose --no-ansi run --rm unit"
                junit testResults: '**/target/surefire-reports/TEST-*.xml', allowEmptyResults: true

                for (def artifact in artifacts) {
                    sh script: "docker image rm ${artifact}:test", returnStatus: true
                    sh "docker-compose --no-ansi build ${artifact}-image"
                }
            }
        }

        withEnv([
                "COMPOSE_FILE=docker/docker-compose-test-local.yml",
                "COMPOSE_PROJECT_NAME=staging"
        ]) {
            stage("Staging") {
                try {
                    sh "docker-compose --no-ansi up -d --build external-service"
                    sh "docker-compose --no-ansi up -d --build internal-service"
                    sleep time: 10, unit: "SECONDS"
                    sh "docker-compose --no-ansi run --rm test"
                } catch (e) {
                    sh "docker-compose --no-ansi logs"
                    error "Staging failed, error: ${e}"
                } finally {
                    sh "docker-compose --no-ansi down"
                }
            }
        }

        def versionOut = sh script: "docker run --rm \
                    -v \$HOME/.m2:/root/.m2 -v \$PWD:/usr/src/app \
                    -w /usr/src/app maven:3.6.0-jdk-8-alpine \
                    mvn -q help:evaluate -Dexpression=project.version -DforceStdout", returnStdout: true
        def version = versionOut.trim()
        echo "Project version: ${version}"

        withEnv([
                "COMPOSE_FILE=docker/docker-compose-test-images.yml",
                "COMPOSE_PROJECT_NAME=publish",
                "TAG=${version}",
                "MVN_PROJ_PROPS=${mavenProjectProperties}"
        ]) {
            stage("Publish") {
                sh "docker-compose --no-ansi run --rm uat"

                for (def service in artifacts) {
                    sh "docker-compose --no-ansi build ${service}-image"
                    sh "docker tag ${service}:${env.TAG} \
                        localhost:5000/${service}:${env.TAG}"
                    sh "docker push \
                        localhost:5000/${service}:${env.TAG}"
                }
            }
        }

        withEnv([
                "DOCKER_TLS_VERIFY=1",
                "DOCKER_HOST_IP=${params['docker.host.ip']}",
                "DOCKER_HOST=tcp://${params['docker.host.ip']}:2376",
                "DOCKER_CERT_PATH=/machines/swarm-1",
                "TAG=${version}"
        ]) {
            stage("Prod-like") {
                // Список сервисов, которые будут развернуты
                def services = [
                        'external-service',
                        'internal-service'
                ]
                def serviceImages = [
                        'external-service': "localhost:5000/external-service:${env.TAG}",
                        'internal-service': "localhost:5000/internal-service:${env.TAG}"
                ]
                def serviceLabels = [
                        'external-service': [
                                'com.df.notify=true',
                                'com.df.scrapePort=8080',
                                'com.df.metricsPath=/external-service/metrics',
                                'com.df.appName=external-service'
                        ],
                        'internal-service': []
                ]
                def serviceMounts = [
                        'external-service': [
                                "${params['docker.mount.root']}/external-service": "/opt/cool-service/external-service/logs",
                        ],
                        'internal-service': [
                                "${params['docker.mount.root']}/internal-service": "/opt/cool-service/internal-service/logs"
                        ],
                ]
                def serviceReplicas = [
                        'external-service': 2,
                        'internal-service': 1
                ]
                def serviceTests = [
                        'external-service': {
                            curl("http://external-service:8080/external-service", "app", 6, 5)
                        },
                        'internal-service': {
                            curl("http://internal-service:8080/internal-service", "app", 6, 5)
                        }
                ]
                def serviceSuccess = [
                        'external-service': {
                            updateProxy("external-service", "/external-service", 8080)
                            echo "Proxy config was updated"
                        },
                        'internal-service': {
                            updateProxy("internal-service", "/internal-service", 8080)
                            echo "Proxy config was updated"
                        }
                ]
                def serviceFailure = [
                        'external-service': {
                        },
                        'internal-service': { e ->
                            rollbackService("internal-service")
                            error "Service internal-service was not deployed, error: ${e}"
                        }
                ]

                // Список сервисов по схеме Blue/Green deployment
                def blueGreenServices = ["external-service"]
                def networks = ["app", "proxy"]

                for (def service in services) {
                    if (blueGreenServices.contains(service)) {
                        deployBlueGreen(service, serviceImages[service], serviceLabels[service], serviceMounts[service],
                                networks, serviceReplicas[service])
                    } else {
                        deployRegular(service, serviceImages[service], serviceLabels[service], serviceMounts[service],
                                networks, serviceReplicas[service], serviceSuccess[service], serviceFailure[service],
                                serviceTests[service])
                    }
                }
            }
        }

        stage("Production") {
        }
    }
}

def curl(String url, String network, int attempts, int interval) {
    int i = attempts
    def succeed = false
    while (i > 0) {
        try {
            sh "docker run \
                --rm \
                --network ${network} \
                appropriate/curl \
                -fgLsS ${url}"
            succeed = true
            break
        } catch (e) {
            --i
            if (i == 0) {
                break
            }
            echo "Test failed, let's try again after ${interval} seconds"
            sleep time: interval, unit: "SECONDS"
        }
    }
    if (!succeed) {
        error "Test failed"
    }
}

def updateProxy(String serviceName, String servicePath, int port) {
    sh "docker run \
        --rm \
        --network proxy \
        appropriate/curl \
        -fgLsS 'http://proxy:8080/v1/docker-flow-proxy/reconfigure?serviceName=${serviceName}&servicePath=${servicePath}&port=${port}'"
}

def rollbackService(String serviceName) {
    sh "docker service update \
        --rollback \
        ${serviceName}"
}