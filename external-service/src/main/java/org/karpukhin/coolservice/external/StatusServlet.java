package org.karpukhin.coolservice.external;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class StatusServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(StatusServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Status");
        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();
        writer.println("OK");
    }
}
