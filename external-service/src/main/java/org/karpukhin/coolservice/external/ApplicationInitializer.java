package org.karpukhin.coolservice.external;

import io.prometheus.client.hotspot.MemoryPoolsExports;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ApplicationInitializer implements ServletContextListener {
    public void contextInitialized(ServletContextEvent sce) {
        new MemoryPoolsExports().register();
    }

    public void contextDestroyed(ServletContextEvent sce) {

    }
}
