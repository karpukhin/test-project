package org.karpukhin.coolservice.external;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

public class HeadersServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(HeadersServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("Headers");
        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();
        for (Enumeration<String> names = req.getHeaderNames(); names.hasMoreElements(); ) {
            String name = names.nextElement();
            writer.println(name + "=" + req.getHeader(name));
        }

        for (Enumeration<String> names = req.getAttributeNames(); names.hasMoreElements(); ) {
            String name = names.nextElement();
            writer.println(name + "=" + req.getAttribute(name));
        }
    }
}
