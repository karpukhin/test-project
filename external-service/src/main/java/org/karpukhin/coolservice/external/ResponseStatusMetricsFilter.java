package org.karpukhin.coolservice.external;

import io.prometheus.client.Counter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ResponseStatusMetricsFilter implements Filter {

    private static final String PATH_COMPONENT_PARAM = "path-components";
    private static final String HELP_PARAM = "help";
    private static final String METRIC_NAME_PARAM = "metric-name";

    private Counter counter = null;

    // Package-level for testing purposes.
    int pathComponents = 1;
    private String metricName = null;
    private String help = "The time taken fulfilling servlet requests";

    public ResponseStatusMetricsFilter() {}

    public ResponseStatusMetricsFilter(
            String metricName,
            String help,
            Integer pathComponents,
            double[] buckets) {
        this.metricName = metricName;
        if (help != null) {
            this.help = help;
        }
        if (pathComponents != null) {
            this.pathComponents = pathComponents;
        }
    }

    private boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }

    private String getComponents(String str) {
        if (str == null || pathComponents < 1) {
            return str;
        }
        int count = 0;
        int i =  -1;
        do {
            i = str.indexOf("/", i + 1);
            if (i < 0) {
                // Path is longer than specified pathComponents.
                return str;
            }
            count++;
        } while (count <= pathComponents);

        return str.substring(0, i);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        Counter.Builder builder = Counter.build()
                .labelNames("path", "method", "status");

        if (filterConfig == null && isEmpty(metricName)) {
            throw new ServletException("No configuration object provided, and no metricName passed via constructor");
        }

        if (filterConfig != null) {
            if (isEmpty(metricName)) {
                metricName = filterConfig.getInitParameter(METRIC_NAME_PARAM);
                if (isEmpty(metricName)) {
                    throw new ServletException("Init parameter \"" + METRIC_NAME_PARAM + "\" is required; please supply a value");
                }
            }

            if (!isEmpty(filterConfig.getInitParameter(HELP_PARAM))) {
                help = filterConfig.getInitParameter(HELP_PARAM);
            }

            // Allow overriding of the path "depth" to track
            if (!isEmpty(filterConfig.getInitParameter(PATH_COMPONENT_PARAM))) {
                pathComponents = Integer.valueOf(filterConfig.getInitParameter(PATH_COMPONENT_PARAM));
            }
        }

        counter = builder
                .help(help)
                .name(metricName)
                .register();
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (!(servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String path = getComponents(request.getRequestURI());

        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            counter.labels(path, request.getMethod(), Integer.toString(response.getStatus())).inc();
        }
    }

    @Override
    public void destroy() {
    }
}
