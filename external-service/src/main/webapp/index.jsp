<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>It works</title>
</head>
<body>
    Request: <%= request.getRequestURI() %><br/>
    Protocol: <%= request.getProtocol() %><br/>
    <h4>Remote</h4>
    Host: <%= request.getRemoteHost() %><br/>
    Address: <%= request.getRemoteAddr() %><br/>
    Scheme: <%= request.getScheme() %><br/>
</body>
</html>
