### Install python:
```
$ ansible-playbook -i inventories/staging/inventory roles/python.yml
```

### Test
```
$ ansible all -i inventories/staging/inventory -m ping
```

### Install Prometheus
```
$ ansible-playbook -i inventories/staging/inventory roles/prometheus.yml
```

### Install Grafana
```
$ ansible-playbook -i inventories/staging/inventory roles/grafana.yml
```
